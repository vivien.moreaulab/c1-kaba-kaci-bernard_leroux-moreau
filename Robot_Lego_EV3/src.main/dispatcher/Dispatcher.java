package dispatcher;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import lejos.hardware.Bluetooth;
import lejos.remote.nxt.BTConnection;
import lejos.remote.nxt.BTConnector;
import lejos.remote.nxt.NXTConnection;
import metier.VehicleController;

/* Cette classe a pour r�le d�une part d'�couter en permanence des ordres potentiels venant de la t�l�commande 
 * et d�autre part de lancer la connexion avec le portail.
 * Les ordres arrivant sont r�ceptionn�s sous forme de nombre � 1 ou 2 chiffres qui sont pass�s dans un switch case.
 * Selon le nombre ainsi re�u, le dispatcher va faire appel � des fonctions du VehicleController.
 * */
public class Dispatcher {

	// Variables d�instances
	Dispatcher myDispatcher;
	VehicleController vehicleController;

	private static DataOutputStream out;
	private static DataInputStream in;
	private static BTConnection BTConnect;
	private static int order = 1000;
	private static boolean stop = true;
	
	public static void main(String[] args) throws InterruptedException, IOException {
		connect();
		VehicleController vehicle = new VehicleController();
		vehicle.start();

		//Boucle qui tourne en permanence
		//Elle permet de lire les messages (sous forme d'entier) envoy�s par la t�l�commande et d'ex�cuter la bonne m�thode de la classe VehicleController
			while (stop) {
				try {
					order = (int) in.readByte();
					switch (order) {
					case 0 :
						vehicle.start();
						break;
					case 21:
						// droite toute vitesse 1
						vehicle.right(10, 300);
						break;
					case 22:
						// droite toute vitesse 2
						vehicle.right(20, 500);
						break;	
					case 23:
						// droite toute vitesse 3
						vehicle.right(30, 700);
						break;	
					case 24:
						// droite toute vitesse 4 (max)
						vehicle.right(40, 900);
						break;	
					case 31:
						// gauche toute vitesse 1
						vehicle.left(10, 300);
						break;
					case 32:
						// gauche toute vitesse 2
						vehicle.left(20, 500);
						break;
					case 33:
						// gauche toute vitesse 3
						vehicle.left(30, 700);
						break;
					case 34:
						// gauche toute vitesse 4 (max
						vehicle.left(40, 900);
						break;
					case 41:
						// en avant toute vitesse 1
						vehicle.forward(10, 300);
						break;
					case 42:
						// en avant toute vitesse 2
						vehicle.forward(20, 500);
						break;
					case 43:
						// en avant toute vitesse 3
						vehicle.forward(30, 700);
						break;
					case 44:
						// en avant toute vitesse 4 (max)
						vehicle.forward(40, 900);
						break;
					case 51:
						// en arri�re toute vitesse 1
						vehicle.backward(10, 300);
						break;
					case 52:
						// en arri�re toute vitesse 2
						vehicle.backward(20, 500);
						break;
					case 53:
						// en arri�re toute vitesse 3
						vehicle.backward(30, 700);
						break;
					case 54:
						// en arri�re toute vitesse 4
						vehicle.backward(40, 900);
						break;
					case 61 : 
						// en avant gauche vitesse 1
						vehicle.leftForward(30, 700);
						break;
					case 62 : 
						// en avant gauche vitesse 4
						vehicle.leftForward(40, 900);
						break;
					case 71 : 
						// en avant droite vitesse 1
						vehicle.rightForward(30, 700);
						break;
					case 72 : 
						// en avant droite vitesse 4
						vehicle.rightForward(40, 900);
						break;
					case 81 : 
						// en arri�re gauche vitesse 1
						vehicle.leftBackward(30, 700);
						break;
					case 82 : 
						// en arri�re gauche vitesse 4
						vehicle.leftBackward(40, 900);
						break;
					case 91 : 
						// en arri�re droite vitesse 1
						vehicle.rightBackward(30, 700);
						break;
					case 92 : 
						// en arri�re droite vitesse 4
						vehicle.rightBackward(40, 900);
						break;
					case 99:
						//-------------------------------------------------------------------------------
						//-----------------------INMPORTANT : Connexion Portail--------------------------
						//----Il faut changer l'adresse IP ci-dessous par celle du point d'acc�s WIFI----
						//-------------------------------------------------------------------------------
						connectClient("192.168.43.212", "80");
						break;
						//-------------------------------------------------------------------------------
						//-----------------------INMPORTANT : Connexion Portail--------------------------
						//----Il faut changer l'adresse IP ci-dessus par celle du point d'acc�s WIFI-----
						//-------------------------------------------------------------------------------
					case 84:
						//Mode automatique
						vehicle.modeAutomatic(40, 900);
						break;
					case 14:
						//arr�t involontaire
						vehicle.stop();
						stop = false;
						in.close();
						out.close();
						break;
					case 9:
						//arr�t volontaire (via bouton quitter)
						vehicle.stop();
						stop = false;
						in.close();
						out.close();
						break;
					default:
						break;
					}
				} catch (IOException ioe) {
					System.out.println("Exception erreur readByte");
					stop = false;
					in.close();
					out.close();
				}
			}
			
			
		}
	
	
	public Dispatcher() {
		vehicleController = new VehicleController();
	}

	public Dispatcher getInstance() {
		if (myDispatcher == null) {
			myDispatcher = new Dispatcher();
		}
		return myDispatcher;
	}

	
	//M�thode permettant la connexion Bluetooth avec la t�l�commande
	public static void connect() {
		System.out.println("En attente");
		//Mise en place de la connection bluetooth
		BTConnector BTconnector = (BTConnector) Bluetooth.getNXTCommConnector();
		BTConnect = (BTConnection) BTconnector.waitForConnection(30000, NXTConnection.RAW);
		//Ouverture des l'�coute et de l'envoi de message
		out = BTConnect.openDataOutputStream();
		in = BTConnect.openDataInputStream();
	}
	
	/**
	 * Connexion Wi-fi avec le portail
	 * M�thode permettant la connexion Wi-Fi avec le portail
	 *
	 * @param hostName : Adresse IP du portail
	 * @param Port   : Port de connexion du portail
	 */
	public static void connectClient(String hostName, String Port) {
       
 
        String hostname = hostName;
        int port = Integer.parseInt(Port);
 
        try (Socket socket = new Socket(hostname, port)) {
 
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
 
            String time = reader.readLine();
 
            System.out.println(time);
 
 
        } catch (UnknownHostException ex) {
 
            System.out.println("Server not found: " + ex.getMessage());
 
        } catch (IOException ex) {
 
            System.out.println("I/O error: " + ex.getMessage());
        }
    }

}
