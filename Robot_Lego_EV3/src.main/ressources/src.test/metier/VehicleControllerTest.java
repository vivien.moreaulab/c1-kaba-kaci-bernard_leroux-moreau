package metier;

import static org.junit.Assert.*;

import org.junit.*;

import ressources.VehicleState;

public class VehicleControllerTest {

	public VehicleController vehicle= new VehicleController();
	
	
	/**
	 * Test setVehicleState()
	 */
	@Test
	public void setVehicleState() {
		
		VehicleState state =  VehicleState.off;
		vehicle.setVehicleState(state);
		assertEquals(state,vehicle.getVehicleState());
	}
}