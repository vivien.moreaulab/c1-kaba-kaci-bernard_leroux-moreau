package metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import ressources.VehicleState;
/**
* Classe DispacherTest.
* Cette classe d�finit l'ensemble des tests unitaires du syst�me.
* @version 1.0
*/
public class DispatcherTest {

	private VehicleController vehicle;
	private UltrasonicSensor ultrasonicSensor;
	private ContactSensor contactSensor;
	private int SpeedA = 10, SpeedM = 300;

	/**
	 * methode ex�cut�e avant chaque test unitaire
	 */
	@Before
	public void init() {
		vehicle = new VehicleController();
	}
	
	/**
	 * test le d�marrage du robot, on verifie que le robot passe bien dans l'�tat neutral
	 */
	@Test
	public void testStart() {
		
		assertEquals(VehicleState.off, vehicle.getVehicleState());

		vehicle.start();
		assertEquals(VehicleState.neutral, vehicle.getVehicleState());
	}

	/**
	 * test le passage du robot � l'etat stop
	 */
	@Test
	public void testStop() {
		
		assertEquals(VehicleState.off, vehicle.getVehicleState());

		vehicle.start();
		vehicle.stop();
		assertEquals(VehicleState.off, vehicle.getVehicleState());

	}

	/**
	 * Methode qui Permet de tester qu�on puisse faire passer le robot 
	 * respectivement de l��tat Forward � l��tat Neutral, cela represente le fait que le v�hicule s'arrete
	 */
	@Test
	public void testStopForward() {

		vehicle.start();
		vehicle.forward(10, SpeedM);
		vehicle.stop();
		assertEquals(VehicleState.off, vehicle.getVehicleState());
	}
	
	/**
	 * Methode qui Permet de tester qu�on puisse faire passer le robot 
	 * respectivement de l��tat backward � l��tat Neutral, cela represente le fait que le v�hicule s'arrete
	 */
	@Test
	public void testStopBackward() {

		vehicle.start();
		vehicle.backward(10, SpeedM);
		vehicle.stop();
		assertEquals(VehicleState.off, vehicle.getVehicleState());
	}

	/**
	 * Permet tester le faire qu�on puisse faire avancer le robot en l'absence d'obstacle
	 */
	@Test
	public void testForward() {
		
		assertEquals(VehicleState.off, vehicle.getVehicleState());
		
		
		vehicle.forward(SpeedA, SpeedM);
		assertEquals(VehicleState.off, vehicle.getVehicleState());

		
		vehicle.start();
		vehicle.forward(SpeedA, SpeedM);
		assertEquals(VehicleState.forward, vehicle.getVehicleState());
		assertEquals(25 + SpeedA, vehicle.getLeftMotor().getSpeed());
		assertEquals(25 + SpeedA, vehicle.getRightMotor().getSpeed());

	}

	/**
	 * Permet tester le faire qu�on puisse faire reculer le robot en l'absence d'obstacle
	 */
	@Test
	public void testBackward() {

		assertEquals(VehicleState.off, vehicle.getVehicleState());
		vehicle.start();
		vehicle.backward(SpeedA, SpeedM);
		assertEquals(VehicleState.backward, vehicle.getVehicleState());
		assertEquals(SpeedA + 25, vehicle.getLeftMotor().getSpeed());
		assertEquals(SpeedA + 25, vehicle.getRightMotor().getSpeed());
	}
	
	/**
	 * Permet de tester le fait que l�on puisse tourner � gauche en l�absence d�obstacle.
	 */
	@Test
	public void testLeft() {

		int speedG,speedD;
		
		vehicle.start();
		
		vehicle.forward(SpeedA, SpeedM);
		
		vehicle.left(SpeedA, SpeedM);

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());

		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedD > speedG);
	}

	/**
	 * Permet de tester le fait que l'on puisse tourner � droite en l�absence d�obstacle.
	 */
	@Test
	public void testRight() {
		
		
		
		int speedG,speedD;
		
		vehicle.start();
		
		vehicle.forward(SpeedA,SpeedM);
		
		vehicle.right(SpeedA,SpeedM);

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());
		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedG > speedD);
	}

	/**
	 * Permet de tester le fait que le v�hicule puisse tourner l�g�rement � droite pendant 
	 * qu�il avance en l'absence d'obstacle
	 */
	@Test
	public void testRightForward() {
		
		
		
		int speedG,speedD;
		

		vehicle.start();
		vehicle.rightForward(SpeedA, SpeedM);
		

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());

		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedG > speedD);
	}
	
	/**
	 * Permet de tester le fait que le v�hicule puisse tourner l�g�rement � gauche pendant 
	 * qu�il avance en l'absence d'obstacle
	 */
	@Test
	public void testLeftForward() {
		
		
		
		int speedG, speedD;
		

		vehicle.start();
		vehicle.leftForward(SpeedA, SpeedM);
		

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());

		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedD > speedG);
	}
	
	/**
	 * Permet de tester le fait que le v�hicule puisse tourner l�g�rement � droite 
	 * pendant qu�il recule en l'absence d'obstacle
	 */
	@Test
	public void testRightBackward() {
		
		
		
		int speedG,speedD;
		

		vehicle.start();
		vehicle.rightBackward(SpeedA, SpeedM);
		

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());

		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedG > speedD);
	}
	
	/**
	 * Permet de tester le fait que le v�hicule puisse tourner l�g�rement � gauche
	 *  pendant qu�il recule en l'absence d'obstacle
	 */
	@Test
	public void testLeftBackward() {
		
		
		
		int speedG, speedD;
		

		vehicle.start();
		vehicle.leftBackward(SpeedA, SpeedM);
		

		speedG = (int) ((int) vehicle.saveSpeedMotorLeft);
		assertEquals(speedG, vehicle.getLeftMotor().getSpeed());

		speedD = (int) ((int) vehicle.saveSpeedMotorRight);
		assertEquals(speedD, vehicle.getRightMotor().getSpeed());
		assertTrue(speedD > speedG);
	}
	
	/**
	 * Permet de tester la fonctionnalit� mettant le robot en �tat d�urgence.
	 */
	@Test
	public void testUrgency() {
		
		vehicle.start();
		vehicle.forward(SpeedA, SpeedM);
		vehicle.urgency();

		assertEquals(0, vehicle.getRightMotor().getSpeed());
		assertEquals(0, vehicle.getLeftMotor().getSpeed());
		assertEquals(VehicleState.urgency, vehicle.getVehicleState());
	}

	/**
	 * Sert � tester la fonctionnalit� permettant de simuler une panne sur le robot.
	 */
	@Test
	public void testBreakdown() {
		
		vehicle.start();
		vehicle.forward(SpeedA, SpeedM);
		vehicle.breakdown();

		assertEquals(0, vehicle.getRightMotor().getSpeed());
		assertEquals(0, vehicle.getLeftMotor().getSpeed());
		assertEquals(VehicleState.breakdown, vehicle.getVehicleState());
	}
	
	/**
	 * Permet de tester le fait que le v�hicule se comporte correctement quand il d�tecte un obstacle.
	 */
	@Test
	public void testultrasonicSensor() {
		ultrasonicSensor = Mockito.mock(UltrasonicSensor.class);
		when(ultrasonicSensor.obstacleDetect()).thenReturn(true);
		
		vehicle.start();
		vehicle.forward(SpeedA, SpeedM);

		assertEquals(VehicleState.off, vehicle.getVehicleState());	
		
	}
	
	/**
	 * Permet de tester le comportement du v�hicule quand il est en contact avec un obstacle
	 */
	@Test
	public void testcontactSensor() {
		contactSensor = Mockito.mock(ContactSensor.class);
		when(contactSensor.obstacleContact()).thenReturn(true);
		
		assertEquals(VehicleState.off, vehicle.getVehicleState());

		vehicle.start();
		vehicle.backward(SpeedA, SpeedM);
		
		assertEquals(VehicleState.off, vehicle.getVehicleState());
		
	}
	

}
