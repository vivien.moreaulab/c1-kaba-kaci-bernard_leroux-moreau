package metier;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import lejos.robotics.RegulatedMotor;

public class MotorTest {
	
	public Motor motor;
	public RegulatedMotor motorLejos;
	
	
	
	@Before
	public void init() {
		
		motor = new Motor( motorLejos);
		
	}

	@Test
	public void setSpeedTest(int speed) {
		
		motor.setSpeed(speed);
		
		assertEquals(speed,motor.getSpeed());
		
	}
	
	@Test
	public void pullTest(int speed) {
		motor.pull(speed);
		assertEquals(speed,motor.getSpeed());
	}
	
	@Test
	public void stopTest() {
	
		motor.stopped();
		assertEquals(0,motor.getSpeed());
		
	}
	
}