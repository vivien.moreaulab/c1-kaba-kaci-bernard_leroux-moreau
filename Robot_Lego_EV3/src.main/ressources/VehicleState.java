package ressources;

/**
 * Classe VehicleState.
 * Cr�er une enumm�ration pour d�finir l'etat du v�hicule EV3.
 * @version 1.0
 */
public enum VehicleState {

	forward, backward, neutral, contact, urgency, breakdown, off,
}
