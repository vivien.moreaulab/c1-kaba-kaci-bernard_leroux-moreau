package metier;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.SensorMode;

/**
* Classe ContactSensor
* Cette classe permet de d�finir le capteur de contact du Robot EV3s
* @version 1.0
*/
/*
 * Cette classe g�re le capteur de contact du robot, celui-ci est utilis� pour la rencontre physique avec un obstacle. 
 * Apr�s avoir d�tect� un obstacle gr�ce � son capteur de contact, elle transmet l�incident au vehiculeControler.
 **/
public class ContactSensor {

	/**
	 * contactSensor : D�finit le capteur de contact.
	 */
	public EV3TouchSensor contactSensor;

	/**
	 * Constructeur de la classe ContactSensor
	 * @param port : le port de la brique EV3
	 */
	public ContactSensor(Port port) {
		this.contactSensor = new EV3TouchSensor(port);
	}
	
	
    /**
     * Permet de savoir si le robot est entr� en contact avec un objet se trouvant devant lui 
     * via un capteur de contact EV3TouchSensor.
     * 
     * @return (true) si il y a contact, (false) sinon.
     */
	public boolean obstacleContact() {
		SensorMode contact = this.contactSensor.getTouchMode();
		float[] sampleContact = new float[contact.sampleSize()];
		contact.fetchSample(sampleContact, 0);
	    float cont = (int) (sampleContact[0] * 100);
	    return cont > 0 ? true : false;
	}
	
}
