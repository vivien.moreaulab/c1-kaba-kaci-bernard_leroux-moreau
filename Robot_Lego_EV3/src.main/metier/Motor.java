package metier;

import lejos.robotics.RegulatedMotor;

/**
 * Classe Motor.
 * Cette classe d�finit le motor EV3.
 * @version 1.0
 */
/*
 * Cette classe re�oit les ordres de VehicleController et permet le d�placement du v�hicule.
 */
public class Motor {

	private RegulatedMotor motorLejos;
	private int speed;

	/**
	 * Constructeur de la classe Moteur.
	 * @param motorLejos : Interface for encoded motors .
	 */
	public Motor(RegulatedMotor motorLejos) {
		this.motorLejos = motorLejos;
		this.speed = 0;
	}

	/**
	 * M�thode qui donne la vitesse du moteur.
	 * @return retourne un entier qui correspond � la vitesse.
	 */
	public int getSpeed() {
		return speed;
	}
	
	/**
	 * M�thode qui permet de modifier la vitesse du moteur.
	 * @param vitesse : un entier qui correspond � la nouvelle vitesse � modifier.
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
		motorLejos.setSpeed(speed);
	}

	/**
	 * Modifie la vitesse pour faire tourner le moteur dans le sens inverse.
	 * @param vitesse : un entier qui correspond � la nouvelle vitesse � modifier.
	 */
	public void pull(int speed) {
		setSpeed(speed);
		motorLejos.backward();
	}

	/**
	 * Modifie la vitesse pour faire tourner le moteur dans le sens.
	 * @param vitesse : un entier qui correspond � la nouvelle vitesse � modifier.
	 */
	public void push(int speed) {
		setSpeed(speed);
		motorLejos.forward();
	}

	/**
	 * @return motorLejos : Interface for encoded motors.
	 */
	public RegulatedMotor getMotorLejos() {
		return motorLejos;
	}

	/**
	 * Modifie l'interface du motor
	 * @param motorLejos : Interface for encoded motors .
	 */
	public void setMotorLejos(RegulatedMotor motorLejos) {
		this.motorLejos = motorLejos;
	}

	/**
	 * Stop le moteur.
	 */
	public void stopped() {
		motorLejos.stop();
		setSpeed(0);
	}
}
