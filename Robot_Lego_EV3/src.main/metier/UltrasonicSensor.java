package metier;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;


/**
 * Classe UltrasonicSensor
 * Cette classe permet de d�finir le capteur d'obstacle du Robot EV3s
 * @version 1.0
 */
/*
 * Cette classe g�re le capteur ultrason du robot, celui-ci est utilis� pour la d�tection d�obstacle � distance.
 * Apr�s avoir d�tect� un obstacle gr�ce � son capteur � ultrasons, elle transmet l�incident au vehiculeControler.
 */
public class UltrasonicSensor {
	
	/**
	 * ultrasonicSensor : D�finit le capteur de pr�sence.
	 */
	public EV3UltrasonicSensor ultrasonicSensor;

	/**
	 * obstacleDistance : Distance de la d�tection de l'obstacle.
	 */
	public final double obstacleDistance=0.20;
	
	
	/**
	 * Constructeur de la classe UltrasonicSensor
	 * @param port : le port de la brique EV3
	 */
	public UltrasonicSensor(Port port) {
		this.ultrasonicSensor = new EV3UltrasonicSensor(port);
	}
	
	/**
	 * M�thode qui d�tecte un obstacle � une distance obstacleDistance
	 * @return (true) si un obstacle est d�tect� � la distace obstacleDistance, (false) sinon.
	 */
	public boolean obstacleDetect() {
		boolean obstacle = false;
		SampleProvider dist = this.ultrasonicSensor.getDistanceMode();
	
		float[] sample = new float[dist.sampleSize()]; 
		int offsetSample = 0;
		
		this.ultrasonicSensor.fetchSample(sample, offsetSample);
		float objectDistance = (float)sample[0];
		
		if(objectDistance < obstacleDistance) {
			obstacle = true;
		}
		return obstacle;
	}
}



	


