package metier;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import ressources.VehicleState;
import lejos.hardware.port.SensorPort;

/**
 * Classe VehicleController. Cette classe d�finit les m�thodes du v�hicule EV3.
 * 
 * @version 1.0
 */

/*
 * Cette classe re�oit des appels de m�thode du dispatcher et re�oit des messages de ses capteurs. 
 * Ses m�thodes ainsi appel�es vont solliciter ses moteurs ou afficher des messages sur la brique.
 * */
public class VehicleController {

	VehicleState vehicleState, saveState;
	int speed, saveSpeedMotorLeft, saveSpeedMotorRight;
	private Motor leftMotor;
	private Motor rightMotor;
	private UltrasonicSensor ultrasonicSensor;
	private ContactSensor contactSensor;
	int speedRange = 25;
	private boolean automatic;
	
	/**
	 * Mise en place du VehicleController avec l'instanciation des moteurs
	 */
	public VehicleController() {
		this.vehicleState = VehicleState.off;
		// Initialisation du capteur.
		this.leftMotor = new Motor(new EV3LargeRegulatedMotor(MotorPort.D));
		this.rightMotor = new Motor(new EV3LargeRegulatedMotor(MotorPort.A));
		this.ultrasonicSensor = new UltrasonicSensor(SensorPort.S2);
		this.contactSensor = new ContactSensor(SensorPort.S1);
		RegulatedMotor T[] = { this.rightMotor.getMotorLejos() };
		this.leftMotor.getMotorLejos().synchronizeWith(T);
		this.automatic=false; 
		speed = 0;
		saveSpeedMotorLeft = 0;
		saveSpeedMotorRight = 0;
	}
	
	/**
	 * Mode automatique
	 * M�thode permettant de faire avancer automatique le robot jusqu'a la d�tection d'un obstacle
	 *
	 * @param speedAdd : vitesse qu'on incr�mente au moteur
	 * @param speedMax : vitesse maximum que peut atteindre le robot en marche avant
	 */
	public void modeAutomatic(int speedAdd, int speedMax) {
		
		while(!ultrasonicSensor.obstacleDetect()) {
			this.forward(speedAdd, speedMax);
			speedAdd++;
			if(speedAdd %3 ==0) {
				speedMax++;
			}		
		}

		this.stop();
		System.out.println("Mode automatique () : Passage au mode manuel");
	
}

	/**
	 * Methode permettant de passer le v�hicule d'�tat off � l'�tat neutral
	 */
	public void start() {
		if (vehicleState == VehicleState.off) {
			vehicleState = VehicleState.neutral;
			System.out.println("start() : Passage de l'etat moteur a neutral");
		} else {
			System.out.println("start() : le vehicule est deja demarre");
		}
	}

	/**
	 * Methode permettant l'arret des moteur et passage a l'etat off.
	 */
	public void stop() {
		// Si l'�tat different de off. On arr�te le moteur?
		if (vehicleState != VehicleState.off) {
			vehicleState = VehicleState.off;
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			System.out.println("stop() : arret du vehicule");
		} else {
			System.out.println("stop() : le vehicule est deja stoppe");
		}
	}

	/**
	 * Methode permettant de faire avancer le robot EV3 vers l'avant.
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void forward(int speedAdd, int speedMax) {
		// On verifie si il y a un obstacle avant de faire avance le robot
		if (!ultrasonicSensor.obstacleDetect()) {
			// Moteur passer de l'etat neutral a forward
			if (vehicleState == VehicleState.neutral) {
				vehicleState = VehicleState.forward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.push(speedRange);
				rightMotor.push(speedRange);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("forward() : le moteur tourne en avant");
				// On v�rifie que le moteur n'atteint pas la vitesse max
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
					rightMotor.setSpeed(rightMotor.getSpeed() + speedAdd);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Si l'�tat est d�j� � forward alors on augmente juste la vitesse des moteurs
			} else if (vehicleState == VehicleState.forward) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed(speed);
				rightMotor.setSpeed(speed);
				leftMotor.getMotorLejos().endSynchronization();
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("forward() : le moteur tourne en avant");
				// On v�rifie que le moteur n'atteint pas la vitesse max
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
					rightMotor.setSpeed(rightMotor.getSpeed() + speedAdd);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Si il �tait en marche arri�re alors, on le fait passer � nouveau en etat
				// forward.
			} else if (vehicleState == VehicleState.backward) {
				leftMotor.stopped();
				rightMotor.stopped();
				vehicleState = VehicleState.forward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.push(speedRange);
				rightMotor.push(speedRange);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
			} else {
				System.out.println("forward() : impossible de faire tourner le moteur en avant");
			}
			// gestion erreur capteur.
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("    Obstacle    ");
			System.out.println("----------------");
		}

	}


	/**
	 * Methode permettant de faire reculer le robot EV3 vers l'avant.
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void backward(int speedAdd, int speedMax) {
		// On verifie que le capteur d'obstacle ne soit pas enclenche avant de reculer.
		if (!contactSensor.obstacleContact()) {
			// Modification de l'etat du vehicule en fonction de son �tat actuel.
			if (vehicleState == VehicleState.neutral) {
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange);
				rightMotor.pull(speedRange);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				// On v�rifie que le moteur n'atteint pas la vitesse max
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
					rightMotor.setSpeed(rightMotor.getSpeed() + speedAdd);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Si l'�tat est d�j� � backward alors on augmente les vitesses des moteurs
			} else if (vehicleState == VehicleState.backward) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed(speed);
				rightMotor.setSpeed(speed);
				leftMotor.getMotorLejos().endSynchronization();
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
					rightMotor.setSpeed(rightMotor.getSpeed() + speedAdd);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Modification de l'etat du vehicule en fonction de son �tat actuel.
			} else if (vehicleState == VehicleState.forward) {
				leftMotor.stopped();
				rightMotor.stopped();
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange);
				rightMotor.pull(speedRange);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				// On v�rifie que le moteur n'atteint pas la vitesse max
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
					rightMotor.setSpeed(rightMotor.getSpeed() + speedAdd);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
			} else {
				System.out.println("backward() : impossible de faire tourner le moteur en arri�re");
			}
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("     Contact    ");
			System.out.println("----------------");
		}

	}

	/**
	 * Methode permettant au robot de tourner vers la gauche.
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void left(int speedAdd, int speedMax) {
		// Si le moteur est en marche avant ou en marche arri�re, alors on fait tourner
		// le robot vers la gauche
		// On augmente plus la vitesse de rotation du moteur droit par rapport a celui
		// du moteur gauche.
		// Ainsi, le robot va tourner vers la gauche
		if (!ultrasonicSensor.obstacleDetect()) {
			vehicleState = VehicleState.forward;
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.push(speedRange);
			rightMotor.push(speedRange * 4);
			leftMotor.getMotorLejos().endSynchronization();
			speed = speedRange;
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			System.out.println("forward() : le moteur tourne en avant");
			if (leftMotor.getSpeed() + speedAdd < speedMax
					&& rightMotor.getSpeed() + speedAdd < speedMax) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed(leftMotor.getSpeed() + speedAdd);
				rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 4);
				leftMotor.getMotorLejos().endSynchronization();
				speed += speedAdd;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("up() : la vitesse du robot augmente");
			} else {
				System.out.println("up() : la vitesse est au maximum");
			}
			System.out.println("left() : modification trajectoire (gauche)");
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("    Obstacle    ");
			System.out.println("----------------");
		}

	}

	/**
	 * Methode permettant au robot de tourner vers la droite.
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void right(int speedAdd, int speedMax) {
		// Si le moteur est en marche avant ou en marche arri�re, alors on fait tourner
		// le robot vers la droite
		// On augmente plus la vitesse de rotation du moteur gauche par rapport a celui
		// du moteur droit.
		// Ainsi, le robot va tourner vers la droite
		if (!ultrasonicSensor.obstacleDetect()) {
			vehicleState = VehicleState.forward;
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.push(speedRange * 4);
			rightMotor.push(speedRange);
			leftMotor.getMotorLejos().endSynchronization();
			speed = speedRange;
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			System.out.println("right() : modification trajectoire (droite)");
			if (leftMotor.getSpeed() + speedAdd < speedMax
					&& rightMotor.getSpeed() + speedAdd < speedMax) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 4);
				rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd));
				leftMotor.getMotorLejos().endSynchronization();
				speed += speedAdd;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("up() : la vitesse du robot augmente");
			} else {
				System.out.println("up() : la vitesse est au maximum");
			}
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("    Obstacle    ");
			System.out.println("----------------");
		}
	}

	/**
	 * Methode permettant de faire avance le robot en avant avec une l�g�re
	 * trajectoire vers la gauche. (Diagonale gauche avant)
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void leftForward(int speedAdd, int speedMax) {
		// On augmente plus la vitesse de rotation du moteur droit par rapport a celui
		// du moteur gauche.
		// A la diff�rence des m�thode pour tourner, la diff�rence d'acc�laration des
		// deux moteurs est moins grande.
		// Le v�hicule a donc une trajectoire ressemblant � une diagonale.
		if (!ultrasonicSensor.obstacleDetect()) {
			vehicleState = VehicleState.forward;
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.push(speedRange * 3);
			rightMotor.push(speedRange * 4);
			leftMotor.getMotorLejos().endSynchronization();
			speed = speedRange;
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			System.out.println("forward() : le moteur tourne en avant");
			if (leftMotor.getSpeed() + speedAdd < speedMax
					&& rightMotor.getSpeed() + speedAdd < speedMax) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 3);
				rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 4);
				leftMotor.getMotorLejos().endSynchronization();
				speed += speedAdd;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("up() : la vitesse du robot augmente");
			} else {
				System.out.println("up() : la vitesse est au maximum");
			}
			System.out.println("left() : modification trajectoire (gauche)");
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("    Obstacle    ");
			System.out.println("----------------");
		}
	}

	/**
	 * Methode permettant de faire avance le robot en avant avec une l�g�re
	 * trajectoire vers la droite. (Diagonale droite avant)
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void rightForward(int speedAdd, int speedMax) {
		// On augmente plus la vitesse de rotation du moteur gauche par rapport a celui
		// du moteur droit.
		// A la diff�rence des m�thode pour tourner, la diff�rence d'acc�laration des
		// deux moteurs est moins grande.
		// Le v�hicule a donc une trajectoire ressemblant � une diagonale.
		if (!ultrasonicSensor.obstacleDetect()) {
			vehicleState = VehicleState.forward;
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.push(speedRange * 4);
			rightMotor.push(speedRange * 3);
			leftMotor.getMotorLejos().endSynchronization();
			speed = speedRange;
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			System.out.println("forward() : le moteur tourne en avant");
			if (leftMotor.getSpeed() + speedAdd < speedMax
					&& rightMotor.getSpeed() + speedAdd < speedMax) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 4);
				rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 3);
				leftMotor.getMotorLejos().endSynchronization();
				speed += speedAdd;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("up() : la vitesse du robot augmente");
			} else {
				System.out.println("up() : la vitesse est au maximum");
			}
			System.out.println("left() : modification trajectoire (gauche)");
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("    Obstacle    ");
			System.out.println("----------------");
		}
	}

	/**
	 * Methode permettant de faire reculer le robot en arrire avec une l�g�re
	 * trajectoire vers la gauche. (Diagonale gauche arri�re)
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void leftBackward(int speedAdd, int speedMax) {
		// On augmente plus la vitesse de rotation du moteur droit par rapport a celui
		// du moteur gauche.
		// A la diff�rence des m�thode pour tourner, la diff�rence d'acc�laration des
		// deux moteurs est moins grande.
		// Le v�hicule a donc une trajectoire ressemblant � une diagonale.
		if (!contactSensor.obstacleContact()) {
			if (vehicleState == VehicleState.neutral) {
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange * 2);
				rightMotor.pull(speedRange * 4);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 2);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 4);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Si l'�tat est d�j� � backward alors on augmente les vitesses des moteurs
			} else if (vehicleState == VehicleState.backward) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed(speed * 2);
				rightMotor.setSpeed(speed * 4);
				leftMotor.getMotorLejos().endSynchronization();
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 2);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 4);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
			} else if (vehicleState == VehicleState.forward) {
				leftMotor.stopped();
				rightMotor.stopped();
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange * 2);
				rightMotor.pull(speedRange * 4);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 2);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 4);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
			} else {
				System.out.println("backward() : impossible de faire tourner le moteur en arri�re");
			}
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("     Contact    ");
			System.out.println("----------------");
		}
	}

	/**
	 * Methode permettant de faire reculer le robot en arrire avec une l�g�re
	 * trajectoire vers la droite. (Diagonale droite arri�re)
	 * 
	 * @param speedAdd : correspond � la vitesse qu'on incr�mente au moteur
	 * @param speedMax   : correspond � la vitesse maximum que peut atteindre le
	 *                     robot en marche avant.
	 */
	public void rightBackward(int speedAdd, int speedMax) {
		// On augmente plus la vitesse de rotation du moteur gauche par rapport a celui
		// du moteur droit.
		// A la diff�rence des m�thode pour tourner, la diff�rence d'acc�laration des
		// deux moteurs est moins grande.
		// Le v�hicule a donc une trajectoire ressemblant � une diagonale.
		if (!contactSensor.obstacleContact()) {
			if (vehicleState == VehicleState.neutral) {
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange * 4);
				rightMotor.pull(speedRange * 2);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 4);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 2);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
				// Si l'�tat est d�j� � backward alors on augmente les vitesses des moteurs
			} else if (vehicleState == VehicleState.backward) {
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.setSpeed(speed * 4);
				rightMotor.setSpeed(speed * 2);
				leftMotor.getMotorLejos().endSynchronization();
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				System.out.println("backward() : le moteur tourne en arriere");
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 4);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 2);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
			} else if (vehicleState == VehicleState.forward) {
				leftMotor.stopped();
				rightMotor.stopped();
				vehicleState = VehicleState.backward;
				leftMotor.getMotorLejos().startSynchronization();
				leftMotor.pull(speedRange * 4);
				rightMotor.pull(speedRange * 2);
				leftMotor.getMotorLejos().endSynchronization();
				speed = speedRange;
				saveSpeedMotorLeft = leftMotor.getSpeed();
				saveSpeedMotorRight = rightMotor.getSpeed();
				if (leftMotor.getSpeed() + speedAdd < speedMax
						&& rightMotor.getSpeed() + speedAdd < speedMax) {
					leftMotor.getMotorLejos().startSynchronization();
					leftMotor.setSpeed((leftMotor.getSpeed() + speedAdd) * 4);
					rightMotor.setSpeed((rightMotor.getSpeed() + speedAdd) * 2);
					leftMotor.getMotorLejos().endSynchronization();
					speed += speedAdd;
					saveSpeedMotorLeft = leftMotor.getSpeed();
					saveSpeedMotorRight = rightMotor.getSpeed();
					System.out.println("up() : la vitesse du robot augmente");
				} else {
					System.out.println("up() : la vitesse est au maximum");
				}
			} else {
				System.out.println("backward() : impossible de faire tourner le moteur en arri�re");
			}
		} else {
			leftMotor.getMotorLejos().startSynchronization();
			leftMotor.stopped();
			rightMotor.stopped();
			leftMotor.getMotorLejos().endSynchronization();
			saveSpeedMotorLeft = leftMotor.getSpeed();
			saveSpeedMotorRight = rightMotor.getSpeed();
			speed = 0;
			vehicleState = VehicleState.neutral;
			System.out.println("----------------");
			System.out.println("     Contact    ");
			System.out.println("----------------");
		}
	}

	/**
	 * Simulation d'une urgence sur le robot
	 */
	public void urgency() {
		// Passage des moteurs � l'arr�t et l'�tat du robot � urgency
		// On conserve la vitesse actuelle des moteurs et l'�tat du moteur
		leftMotor.getMotorLejos().startSynchronization();
		saveSpeedMotorLeft = leftMotor.getSpeed();
		saveSpeedMotorRight = rightMotor.getSpeed();
		leftMotor.stopped();
		rightMotor.stopped();
		leftMotor.getMotorLejos().endSynchronization();
		saveState = vehicleState;
		vehicleState = VehicleState.urgency;
		System.out.println("urgency() : le vehicule se bloque");
	}

	/**
	 * Simulation d'une panne sur le robot
	 */
	public void breakdown() {
		// Passage des moteurs � l'arr�t et l'�tat du robot � panne
		// On conserve la vitesse actuelle des moteurs et l'�tat du moteur
		leftMotor.getMotorLejos().startSynchronization();
		saveSpeedMotorLeft = leftMotor.getSpeed();
		saveSpeedMotorRight = rightMotor.getSpeed();
		leftMotor.stopped();
		rightMotor.stopped();
		leftMotor.getMotorLejos().endSynchronization();
		saveState = vehicleState;
		vehicleState = VehicleState.breakdown;
		System.out.println("breakdown() : Simulation de panne");
	}

	/**
	 * Restauration des anciennes valeurs de la vitesse et de l'�tat du robot
	 * M�thode qui est appel� apr�s un �tat de panne ou urgency
	 */
	public void restore() {
		leftMotor.getMotorLejos().startSynchronization();
		leftMotor.setSpeed(saveSpeedMotorLeft);
		rightMotor.setSpeed(saveSpeedMotorRight);
		leftMotor.getMotorLejos().endSynchronization();
		vehicleState = saveState;
		System.out.println("restore() : le v�hicule restaure son �tat");
	}

	/**
	 * Retourne l'etat du v�hicule
	 * 
	 * @return VehicleState
	 */
	public VehicleState getVehicleState() {
		return vehicleState;
	}

	/**
	 * Modifie l'etat du v�hicule
	 * 
	 * @param vehicleState
	 */
	public void setVehicleState(VehicleState vehicleState) {
		this.vehicleState = vehicleState;
	}

	/**
	 * Retourne la vitesse
	 * 
	 * @return entier
	 */
	public int getSpeedRange() {
		return speedRange;
	}

	/**
	 * Modifie la vitesse
	 * 
	 * @param speedRange
	 */
	public void setSpeedRange(int speedRange) {
		this.speedRange = speedRange;
	}

	/**
	 * Retourne le moteur gauche
	 * 
	 * @return Motor
	 */
	public Motor getLeftMotor() {
		return leftMotor;
	}

	/**
	 * Modifie la valeur du moteur gauche.
	 * 
	 * @param leftMotor
	 */
	public void setLeftMotor(Motor leftMotor) {
		this.leftMotor = leftMotor;
	}

	/**
	 * Retourne le moteur droit
	 * 
	 * @return Motor
	 */
	public Motor getRightMotor() {
		return rightMotor;
	}

	/**
	 * Modifie la valeur du moteur droit.
	 * 
	 * @param rightMotor
	 */
	public void setRightMotor(Motor rightMotor) {
		this.rightMotor = rightMotor;
	}
}
